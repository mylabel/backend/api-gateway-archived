# API Gateway :lock:

## Что такое API Gateway?

API gateway – это умный proxy-сервер между пользователями и любым количеством сервисов (API), являющийся по своей сути точкой "входа" в микросервисную архитектуру.

## Возможности

- Динамическая расширяемая авторизация
    - Возможность создания неограниченного количества ролей с необходимыми разрешениями
    - Возможность выдачи нескольких ролей одному пользователю
- Настраиваемые файлы конфигурации
    - Возможность изменения маршрутов без вмешательства в исходный код
    - Возможность расширения функционала со всеми возможными методами запросов (POST, GET, PATCH, PUT, DELETE)

## Использование

1. Склонируйте репозиторий на локальную машину
    ```bash
    $ git clone https://gitlab.com/Wedyarit/api-gateway.git
    ```

2. Установите все зависимости
   ```bash
   $ composer install
   ```

3. Настройте файл переменных среды (.env) и [файл конфигураций API Gateway](#файл-конфигураций)

4. Запустите проект
    ```bash
   $ php -S localhost:* -t ./public
    ```

## Файл конфигураций

[Файл конфигураций](https://gitlab.com/Wedyarit/api-gateway/-/blob/master/config/gateway.php) разделен на две секции:

- [routes](#маршруты) (маршруты)

### Маршруты

Пример секции маршрутов:

```php
[
    'routes' => [
        [
            'method' => 'GET',
            'path' => 'users',
            'permission' => 'view-users',
            'authentication' => true,
            'action' => [
                'service' => 'auth',
                'method' => 'GET',
                'path' => 'users',
            ]
        ],
        [
            'method' => 'GET',
            'path' => 'user/{uuid}',
            'permission' => 'view-user',
            'authentication' => true,
            'action' => [
                'service' => 'auth',
                'method' => 'GET',
                'path' => 'user/{uuid}',
            ]
        ],
        [
            'method' => 'POST',
            'path' => 'user',
            'permission' => 'create-user',
            'authentication' => false,
            'action' => [
                'service' => 'auth',
                'method' => 'POST',
                'path' => 'user',
            ]
        ]
    ]
];
```

<br>

Рассмотрим один маршрут более предметно:

```php
[
    'method' => 'GET',
    'path' => 'users',
    'permission' => 'view-users',
    'authentication' => true,
    'action' => [
        'service' => 'auth',
        'method' => 'GET',
        'path' => 'users',
    ]
];
```

Свойства маршрута:

- <b>method</b><br>Данное свойство описывает метод запроса (существующие методы: GET, POST, PUT, PATCH, DELETE)<br><br>
- <b>path</b><br>Данное свойство описывает endpoint запроса (т.е. в данном примере, чтобы получить верный ответ, клиент должен сделать запрос на `http://api.gateway/users`, где `http://api.gateway`- адрес шлюза)<br><br>
- <b>description</b><br>Данное свойство описывает суть запроса. Обязательный параметр, необходимый для интуитивного определения цели маршрута<br><br>
- <b>permission</b><br>Данное свойство описывает разрешение (см. подробнее [разрешения](#разрешения)), необходимого для запроса. В случае невыполнения условия, будет возвращено `401 (Unauthorized)`<br><br>
- <b>authentication</b><br>Данное свойство описывает необходимость в аутентификации при выполнении запроса. <b>В случае, если аутентификация не играет роли, поле не нужно указывать.</b> В случае невыполнения условия, будет возвращено `401 (Unauthorized)`<br><br>
- <b>verification</b><br>Данное свойство описывает необходимость в верификации при выполнении запроса. <b>В случае, если верификация не играет роли, поле не нужно указывать.</b> В случае невыполнения условия, будет возвращено `401 (User is not verified)`<br><br>
- <b>action</b><br>Данное свойство описывает маршрут, на который будет выполнена переадресация в случае успешного запроса<br><br>
    - <b>service</b><br>Данное свойство описывает название микросервиса, на который будет совершен запрос. Название сервиса должно соответствовать указанному в секции сервисов (см. подробнее [сервисы](#сервисы))<br><br>
    - <b>method</b><br>Данное свойство описывает метод запроса (существующие методы: GET, POST, PUT, PATCH, DELETE)<br><br>
    - <b>path</b><br>Данное свойство описывает endpoint запроса (т.е. в данном примере, чтобы получить верный ответ, шлюз должен сделать запрос на `http://auth.service/users`, где `http://auth.service`- адрес микросервиса, указанный в `hostname` сервиса в [секции сервисов](#сервисы))<br><br>

Более подробная информация про свойства `authentication` и `verification`:

- В случае, если поле `permissions` задано, `authentication` автоматически будет <b>true</b>
- В случае, если поле `authentication` или  `verification` равно <b>true</b>, получить доступ к маршруту смогут только аутентифицированные (вошедшие в систему) пользователи
- В случае, если поле `authentication` равно <b>false</b>, получить доступ к маршруту смогут только <b>не</b> аутентифицированные (<b>не</b> вошедшие в систему) пользователи
- В случае, если поле `verification` равно <b>false</b>, получить доступ к маршруту смогут только <b>не</b> верифицированные (<b>не</b> подтвердившие почту) пользователи
- В случае, если поле `authentication` и `verification` <b>не задано</b>, получить доступ к маршруту смогут <b>все</b> пользователи

В случае, если в запрос необходимо передать query-параметры, их необходимо заключить в фигурные (`{}`) скобки.

Пример:

```php
[
    'method' => 'GET',
    'path' => 'user/{uuid}',
    'description' => 'View all users',
    'permission' => 'view-user',
    'action' => [
        'service' => 'auth',
        'method' => 'GET',
        'path' => 'user/{uuid}',
    ]
];
```

<i>В запросе необходимо передать UUID пользователя.
<br>Поле `authentication` отсутствует, т.к. нам неважно: аутентифицирован пользователь или нет</i>

## Разрешения

Для контроля прав и авторизации предназначены разрешения (permissions). Разрешение определяет, к какому маршруту клиент может получить доступ. Как правило, одно разрешение описывает один маршрут.

Пример структуры разрешений:

| ID | Name | Description |
| ------ | ------ | ------ |
1|view-role|Permission to view a specific role
2|create-role|Permission to create role
3|view-role-permissions|Permission to view role permissions
4|replace-role|Permission to replace role
5|update-role|Permission to update role
6|view-roles|Permission to view roles
7|view-permission|Permission to view a specific permission
8|create-permissions|Permission to create permission
9|replace-permission|Permission to replace permission
10|update-permission|Permission to update permission
11|view-permission|Permission to view permissions
12|give-role|Permission to issue a role to a user
13|remove-role|Permission to remove a role from a user
14|view-user-roles|Permission to view user roles
15|give-permission|Permission to grant permissions to a role
16|remove-permission|Permission to revoke permission from a role
17|view-user|Permission to view specific user
18|view-users|Permission to view users
19|create-user|Permission to create user
20|replace-user|Permission to replace user
21|update-user|Permission to update user
22|create-artist|Permission to create artist
23|view-artist|Permission to view specific artist
24|add-user-artist|Permission to add user to artist
25|view-artists|Permission to view artists
26|view-own-artists|Permission to view own artists

<br>

### Взаимодействие с разрешениями

<b>Обратите внимание!</b> Генерация разрешений происходит на автоматическом уровне при миграциях базы данных. Необдуманное редактирование, удаление и создание разрешений могут привести к сбоям и/или некорректной работе системы.

`GET`⠀⠀/permission/{permission_id} <br>
Получение информации о разрешении <br>
Необходимое разрешение: `view-permission`

<br>`GET`⠀⠀/permissions <br>
Получение полного списка разрешений <br>
Необходимое разрешение: `view-permissions`

<br>`POST`⠀⠀/permission <br>
Создание разрешения.<br>
Необходимое разрешение: `create-permission`

```json 
{
    "name": "string",
    "description": "string"
}
```

<br>`PUT`⠀⠀/permission/{id} <br>
Полное обновление (замена) разрешения<br>
Необходимое разрешение: `replace-permission`

```json 
{
    "name": "string",
    "description": "string"
}
```

<br>`PATCH`⠀⠀/permission/{id} <br>
Частичное обновление разрешения<br>
Необходимое разрешение: `update-permission`

```json 
{
    "name": "?string",
    "description": "?string"
}
```

<br>`DELETE`⠀⠀/permission/{permission_id} <br>
Удаление разрешения<br>
Необходимое разрешение: `delete-permission`

## Роли

Роли (roles) представляют собой набор определенных разрешений, что предоставляет неограниченную гибкость и расширяемость приложения.

### Взаимодействие с ролями

`GET`⠀⠀/role/{role_id} <br>
Получение информации о роли<br>
Необходимое разрешение: `view-role`

<br>`GET`⠀⠀/roles <br>
Получение полного списка ролей<br>
Необходимое разрешение: `view-roles`

<br>`GET`⠀⠀/role/{role_id}/permissions <br>
Получение полного списка разрешений роли<br>
Необходимое разрешение: `view-role-permissions`

<br>`GET`⠀⠀/user/{user_uuid}/roles <br>
Получение полного списка ролей пользователя<br>
Необходимое разрешение: `view-user-roles`

<br>`POST`⠀⠀/role <br>
Создание роли<br>
Необходимое разрешение: `create-role`

```json 
{
    "name": "string",
    "description": "string"
    "acess_level": "int >= 0"
}
```

<br>`PUT`⠀⠀/role/{id} <br>
Полное обновление (замена) роли<br>
Необходимое разрешение: `replace-role`

```json 
{
    "name": "string",
    "description": "string"
    "acess_level": "int >= 0"
}
```

<br>`PATCH`⠀⠀/role/{id} <br>
Частичное обновление роли<br>
Необходимое разрешение: `update-role`

```json 
{
    "name": "?string",
    "description": "?string"
    "acess_level": "?int >= 0"
}
```

<br>`DELETE`⠀⠀/role/{role_id} <br>
Удаление роли<br>
Необходимое разрешение: `delete-role`

### Уровень доступа

Уровень доступа (access level) предназначен для выборочного взаимодействия с данными, исходя из роли пользователя. Данный подход позволяет фильтровать данные, что повышает общую оптимизацию приложения, и создает дополнительный слой защиты данных.

Общая информация:

- Минимальный уровень доступа - 0, данный уровень доступа характерен для <b>не</b> аутентифицированных пользователей (<b>не</b>  вошедших в систему)
- Каждая роль обладает своим уровнем доступа
- Чем выше уровень доступа, тем большим контролем обладает роль
- Уровень доступа передается в заголовке (header) запроса от API Gateway к микросервису

Уровни доступа определяются в моделях каждого микросервиса как константное значение:

```php
const accessLevels = [
    'get' => [
        0 => ['name'],
        1 => ['name, email'],
        2 => ['name', 'email', 'phone_number'],
        3 => ['name', 'email', 'phone_number', 'password'],
        4 => ['*']
    ],
    'update' => [
        0 => ['name'],
        1 => ['name, email'],
        2 => ['name', 'email', 'phone_number'],
        3 => ['name', 'email', 'phone_number', 'password'],
        4 => ['*']
    ]
];
```

## Выдача ролей, разрешений

`GET`⠀⠀/user/{user_uuid}/role/{role_id} <br>Выдача роли пользователю<br>
Необходимое разрешение: `give-role`

<br>`DELETE`⠀⠀/user/{user_uuid}/role/{role_id} <br>Лишение роли пользователя<br>
Необходимое разрешение: `remove-role`

<br>`GET`⠀⠀/role/{role_id}/permission/{permission_id} <br>Выдача разрешения роли<br>
Необходимое разрешение: `give-permission`

<br>`DELETE`⠀⠀ /role/{role_id}/permission/{permission_id} <br>Лишение разрешения роли<br>
Необходимое разрешение: `remove-permission`

<br>

---
<br>
<p align="center"><i>~ API-Gateway microservice unit ~</i></p>
