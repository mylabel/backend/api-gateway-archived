<?php

namespace App\Http;

use App\Routing\Route;
use App\Routing\RouteContract;

class Request extends \Illuminate\Http\Request
{
    protected RouteContract $currentRoute;

    public function attachRoute(RouteContract $route): Request
    {
        $this->currentRoute = $route;

        return $this;
    }

    public function getRoute(): Route
    {
        return $this->currentRoute;
    }

    public function route($param = null, $default = null)
    {
        $route = call_user_func($this->getRouteResolver());

        if (is_null($route) || is_null($param)) {
            return $route;
        } else {
            return $route[2][$param];
        }
    }

    public function getRouteParams(): array
    {
        $route = call_user_func($this->getRouteResolver());

        return $route ? $route[2] : [];
    }
}
