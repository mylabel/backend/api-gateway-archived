<?php

namespace App\Http\Middleware;

use App\Helpers\JWTManager;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use phpcommon\Handler\Exceptions\USER_VERIFICATION_EXCEPTION;
use phpcommon\Utils\ServiceRegistry;
use PHPUnit\Util\Json;
use Throwable;


class VerificationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return Json|JsonResponse|Response|null
     * @throws Throwable
     */
    public function handle(Request $request, Closure $next): Json|JsonResponse|Response|null
    {
        throw_unless((Http::get(ServiceRegistry::getAddress('Auth-Microservice') . '/user/' . (JWTManager::parse($request->bearerToken())->payload->user_uuid) . '/is-verified')->json(['data'])), new USER_VERIFICATION_EXCEPTION);

        return $next($request);
    }
}
