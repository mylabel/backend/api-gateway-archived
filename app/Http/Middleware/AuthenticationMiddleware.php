<?php

namespace App\Http\Middleware;

use App\Helpers\JWTManager;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;
use Throwable;

class AuthenticationMiddleware
{
    /**
     * @throws Throwable
     */
    public function handle(Request $request, Closure $next, bool $authentication)
    {
        // Проверка корректности токена
        if (!$userUUID = JWTManager::parseJWT($request)) {
            if (!$authentication)
                return $next($request);
            throw new UnauthorizedException;
        }

        if ($authentication) {
            $request->attributes->add(['user_uuid' => $userUUID]);
            return $next($request);
        }

        throw new UnauthorizedException();
    }

}
