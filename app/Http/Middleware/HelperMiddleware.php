<?php

namespace App\Http\Middleware;

use App\Http\Request;
use App\Routing\RouteRegistry;
use Closure;

class HelperMiddleware
{
    public function handle(Request $request, Closure $next, string $id): mixed
    {
        $request->attachRoute(
            app()->make(RouteRegistry::class)->getRoute($id)
        );

        return $next($request);
    }
}
