<?php

namespace App\Http\Middleware;

use App\Helpers\JWTManager;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\UnauthorizedException;
use phpcommon\Utils\MicroServices;
use phpcommon\Utils\ServiceRegistry;
use PHPUnit\Util\Json;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Throwable;


class SubscriptionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return Json|JsonResponse|Response|null
     * @throws Throwable
     */
    public function handle(Request $request, Closure $next): Json|JsonResponse|Response|null
    {
        throw_unless($userUUID = JWTManager::parseJWT($request), new UnauthorizedException);

        $remainder = Http::post(ServiceRegistry::getAddress(MicroServices::SUBSCRIPTIONS_MICROSERVICE) . '/subscription-status', [
            'route' => $request->path(),
            'user_uuid' => $userUUID
        ])->json(['data', 'status']);

        throw_unless($remainder, new AccessDeniedException());

        return $next($request);
    }
}
