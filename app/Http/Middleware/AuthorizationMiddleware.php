<?php

namespace App\Http\Middleware;

use App\Helpers\JWTManager;
use App\Models\Role;
use App\Models\RolePermission;
use App\Models\UserRole;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Throwable;

class AuthorizationMiddleware
{
    /**
     * @throws Throwable
     */
    public function handle(Request $request, Closure $next, string $permissionName)
    {
        // Проверка существования разрешения
        throw_unless($permission = DB::table('permissions')->where('name', $permissionName)->first(), new BadRequestException);

        throw_unless($userUUID = JWTManager::parseJWT($request), new UnauthorizedException);

        // Получение ролей пользователя
        $user_roles = UserRole::where('user_uuid', $userUUID)->get();

        // Проверка существования permission
        foreach ($user_roles as $user_role) {
            $role = Role::where('id', $user_role->role_id)->first();

            foreach ($role->permissions as $userPermission) {
                if ($permission->id === $userPermission->id) {
                    $request->attributes->add(['access_level' => $role->access_level]);
                    return $next($request);
                }
            }
        }

        throw new AccessDeniedException();
    }
}
