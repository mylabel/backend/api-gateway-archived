<?php

namespace App\Http\Controllers;

use App\Http\Request;
use App\Routing\Action;
use App\Services\RestClient;
use JetBrains\PhpStorm\Pure;
use Laravel\Lumen\Http\ResponseFactory;
use Laravel\Lumen\Routing\Controller;

class GatewayController extends Controller
{
    protected Action $action;

    protected array $config;

    #[Pure] public function __construct(Request $request)
    {
        $this->config = $request->getRoute()->getConfig();
        $this->action = $request->getRoute()->getAction();
    }

    public function get(Request $request, RestClient $client): \Illuminate\Http\Response|ResponseFactory
    {
        $response = $this->request($request, $client);
        return \response($response, $response->status())->withHeaders($response->headers());
    }

    private function request(Request $request, RestClient $client)
    {
        $client->setAccessLevel($request->get('access_level'));
        if ($request->get('user_uuid'))
            $client->setUserUUID($request->get('user_uuid'));
        if (count($request->input()) > 0)
            $client->setBody($request->input());

        if (count($request->allFiles()) > 0) {
            $client->setFiles($request->allFiles());
        }

        $parametersJar = array_merge($request->getRouteParams(), ['query_string' => $request->getQueryString()]);

        return $client->syncRequest($this->action, $parametersJar);
    }

    public function delete(Request $request, RestClient $client): \Illuminate\Http\Response|ResponseFactory
    {
        $response = $this->request($request, $client);
        return \response($response, $response->status())->withHeaders($response->headers());
    }

    public function post(Request $request, RestClient $client): \Illuminate\Http\Response|ResponseFactory
    {
        $response = $this->request($request, $client);
        return \response($response, $response->status())->withHeaders($response->headers());
    }

    public function put(Request $request, RestClient $client): \Illuminate\Http\Response|ResponseFactory
    {
        $response = $this->request($request, $client);
        return \response($response, $response->status())->withHeaders($response->headers());
    }

    public function patch(Request $request, RestClient $client): \Illuminate\Http\Response|ResponseFactory
    {
        $response = $this->request($request, $client);
        return \response($response, $response->status())->withHeaders($response->headers());
    }
}
