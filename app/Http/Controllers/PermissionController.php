<?php

namespace App\Http\Controllers;

use App\Services\PermissionService;
use App\Services\RoleService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use phpcommon\Handler\Exceptions\ENTITY_EXISTS_EXCEPTION;
use phpcommon\http\Messages\ENTITY_CREATED_Message;
use phpcommon\http\Messages\ENTITY_UPDATED_Message;
use phpcommon\http\Messages\SUCCESSFUL_REQUEST_Message;
use phpcommon\http\ResponseMessagesDTO as ResponseDTO;
use phpcommon\http\ResponseProvider;
use Throwable;


class PermissionController extends Controller
{
    private array $permissionRequestRules = ['name' => 'required|min:3|max:255', 'description' => 'min:3|max:255'];

    private PermissionService $permissionService;
    private RoleService $roleService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PermissionService $permissionService, RoleService $roleService)
    {
        $this->permissionService = $permissionService;
        $this->roleService = $roleService;
    }


    /**
     * @throws ValidationException
     * @throws Throwable
     */
    public function item($permission_id): JsonResponse
    {
        // Проверка существования разрешения
        throw_unless($permission = $this->permissionService->find(['id' => $permission_id], -1), new ModelNotFoundException);

        // Возвращение успешного ответа
        return ResponseProvider::render(new ResponseDTO(new SUCCESSFUL_REQUEST_Message, [
            'name' => $permission->name,
            'description' => $permission->description
        ]));
    }


    /**
     * @throws ValidationException
     * @throws Throwable
     */
    public function delete($permission_id): JsonResponse
    {
        // Проверка существования разрешения
        throw_unless($this->permissionService->destroy(['id' => $permission_id], -1), new ModelNotFoundException);

        // Возвращение успешного ответа
        return ResponseProvider::render(new ResponseDTO(new SUCCESSFUL_REQUEST_Message));
    }

    /**
     * @throws Throwable
     */
    public function all(): JsonResponse
    {
        // Возвращение успешного ответа
        return ResponseProvider::render(new ResponseDTO(new SUCCESSFUL_REQUEST_Message(), $this->permissionService->all(-1)));
    }


    /**
     * @throws Throwable
     */
    public function list($role_id): JsonResponse
    {
        // Возвращение успешного ответа
        return ResponseProvider::render(new ResponseDTO(new SUCCESSFUL_REQUEST_Message(), $this->permissionService->getRolePermissions($role_id)));
    }


    /**
     * @throws ValidationException
     * @throws Throwable
     * @throws Throwable
     */
    public function create(Request $request): JsonResponse
    {
        // Валидация входных данных
        $validatedData = $this->validate($request, $this->permissionRequestRules);

        // Создание разрешения
        $permission = $this->permissionService->create([
            'name' => $validatedData['name'],
            'description' => $validatedData['description']
        ], -1);

        // Возвращение успешного ответа
        return ResponseProvider::render(new ResponseDTO(new ENTITY_CREATED_Message,
            ['permission' => [
                'name' => $permission->name,
                'description' => $permission->description,
            ]]
        ));
    }


    /**
     * @throws ValidationException
     * @throws Throwable
     */
    public function update(Request $request, string $id): JsonResponse
    {
        // Валидация входных данных
        $validatedData = $this->validate($request, [
            'description' => 'min:3|max:255',
        ]);

        // Обновление разрешения
        throw_unless($this->permissionService->update('id', $id, $validatedData, -1), new ModelNotFoundException);

        // Возвращение успешного ответа
        return ResponseProvider::render(new ResponseDTO(new ENTITY_UPDATED_Message));
    }


    /**
     * @throws ValidationException
     * @throws Throwable
     */
    public function replace(Request $request, string $id): JsonResponse
    {
        // Валидация входных данных
        $validatedData = $this->validate($request, $this->permissionRequestRules);

        // Обновление разрешения
        throw_unless($this->permissionService->update('id', $id, $validatedData, -1), new ModelNotFoundException);

        // Возвращение успешного ответа
        return ResponseProvider::render(new ResponseDTO(new ENTITY_UPDATED_Message));
    }

    /**
     * @throws Throwable
     */
    public function give($role_id, $permission_id): JsonResponse
    {
        // Проверка существования роли
        throw_unless($role = $this->roleService->find(['id' => $role_id], -1), new ModelNotFoundException);
        // Проверка существования разрешения
        throw_unless($permission = $this->permissionService->find(['id' => $permission_id], -1), new ModelNotFoundException);
        // Проверка преждевременного наличия разрешения у роли
        throw_if($this->permissionService->hasRolePermission($role->id, $permission->id), new ENTITY_EXISTS_EXCEPTION);

        // Выдача роли пользователю
        $this->permissionService->givePermission($role->id, $permission->id);

        // Возвращение успешного ответа
        return ResponseProvider::render(new ResponseDTO(new ENTITY_UPDATED_Message));
    }


    /**
     * @throws Throwable
     */
    public function remove($role_id, $permission_id): JsonResponse
    {
        // Проверка существования роли
        throw_unless($role = $this->roleService->find(['id' => $role_id], -1), new ModelNotFoundException);
        // Проверка существования разрешения
        throw_unless($permission = $this->permissionService->find(['id' => $permission_id], -1), new ModelNotFoundException);
        // Проверка преждевременного наличия разрешения у роли
        throw_unless($this->permissionService->hasRolePermission($role->id, $permission->id), new ModelNotFoundException);

        // Удаление роли у пользователю
        $this->permissionService->removePermission($role->id, $permission->id);

        // Возвращение успешного ответа
        return ResponseProvider::render(new ResponseDTO(new ENTITY_UPDATED_Message));
    }
}
