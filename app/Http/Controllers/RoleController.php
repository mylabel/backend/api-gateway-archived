<?php

namespace App\Http\Controllers;

use App\Services\RoleService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\ValidationException;
use phpcommon\Handler\Exceptions\ENTITY_EXISTS_EXCEPTION;
use phpcommon\http\Messages\ENTITY_CREATED_Message;
use phpcommon\http\Messages\ENTITY_UPDATED_Message;
use phpcommon\http\Messages\SUCCESSFUL_REQUEST_Message;
use phpcommon\http\ResponseMessagesDTO as ResponseDTO;
use phpcommon\http\ResponseProvider;
use phpcommon\Utils\ServiceRegistry;
use Throwable;

class RoleController extends Controller
{
    private array $roleRequestRules = ['name' => 'required|min:3|max:255', 'description' => 'min:3|max:255', 'access_level' => 'required|digits_between:0,5'];

    private RoleService $roleService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    /**
     * @throws ValidationException
     * @throws Throwable
     */
    public function item($role_id): JsonResponse
    {
        // Проверка существования разрешения
        throw_unless($role = $this->roleService->find(['id' => $role_id], -1), new ModelNotFoundException);

        // Возвращение успешного ответа
        return ResponseProvider::render(new ResponseDTO(new SUCCESSFUL_REQUEST_Message, [
            'name' => $role->name,
            'description' => $role->description
        ]));
    }

    /**
     * @throws ValidationException
     * @throws Throwable
     */
    public function delete($role_id): JsonResponse
    {
        // Проверка существования разрешения
        throw_unless($this->roleService->destroy(['id' => $role_id], -1), new ModelNotFoundException);

        // Возвращение успешного ответа
        return ResponseProvider::render(new ResponseDTO(new SUCCESSFUL_REQUEST_Message));
    }


    /**
     * @throws Throwable
     */
    public function all(): JsonResponse
    {
        // Возвращение успешного ответа
        return ResponseProvider::render(new ResponseDTO(new SUCCESSFUL_REQUEST_Message(), $this->roleService->all(-1)));
    }


    /**
     * @throws Throwable
     */
    public function list($user_uuid): JsonResponse
    {
        // Возвращение успешного ответа
        return ResponseProvider::render(new ResponseDTO(new SUCCESSFUL_REQUEST_Message(), $this->roleService->getUserRoles($user_uuid)));
    }


    /**
     * @throws ValidationException
     * @throws Throwable
     * @throws Throwable
     */
    public function create(Request $request): JsonResponse
    {
        // Валидация входных данных
        $validatedData = $this->validate($request, $this->roleRequestRules);

        // Создание разрешения
        $role = $this->roleService->create([
            'name' => $validatedData['name'],
            'description' => $validatedData['description'],
            'access_level' => $validatedData['access_level'],
        ], -1);

        // Возвращение успешного ответа
        return ResponseProvider::render(new ResponseDTO(new ENTITY_CREATED_Message,
            ['role' => [
                'name' => $role->name,
                'description' => $role->description,
            ]]
        ));
    }


    /**
     * @throws ValidationException
     * @throws Throwable
     */
    public function update(Request $request, string $id): JsonResponse
    {
        // Валидация входных данных
        $validatedData = $this->validate($request, [
            'description' => 'min:3|max:255',
        ]);

        // Обновление разрешения
        throw_unless($this->roleService->update('id', $id, $validatedData, -1), new ModelNotFoundException);

        // Возвращение успешного ответа
        return ResponseProvider::render(new ResponseDTO(new ENTITY_UPDATED_Message));
    }


    /**
     * @throws ValidationException
     * @throws Throwable
     */
    public function replace(Request $request, string $id): JsonResponse
    {
        // Валидация входных данных
        $validatedData = $this->validate($request, $this->roleRequestRules);

        // Обновление разрешения
        throw_unless($this->roleService->update('id', $id, $validatedData, -1), new ModelNotFoundException);

        // Возвращение успешного ответа
        return ResponseProvider::render(new ResponseDTO(new ENTITY_UPDATED_Message));
    }

    /**
     * @throws Throwable
     */
    public function give($user_uuid, $role_id): JsonResponse
    {
        // Проверка существования пользователя
        throw_unless((Http::get(ServiceRegistry::getAddress('Auth-Microservice') . '/user/' . $user_uuid . '/exists')->json(['data'])), new ModelNotFoundException);

        // Проверка существования роли
        throw_unless($role = $this->roleService->find(['id' => $role_id], -1), new ModelNotFoundException);
        // Проверка преждевременного наличия роли у пользователя
        throw_if($this->roleService->hasUserRole($user_uuid, $role->id), new ENTITY_EXISTS_EXCEPTION);

        // Выдача роли пользователю
        $this->roleService->giveRole($user_uuid, $role->id);

        // Возвращение успешного ответа
        return ResponseProvider::render(new ResponseDTO(new ENTITY_UPDATED_Message));
    }


    /**
     * @throws Throwable
     */
    public function remove($user_uuid, $role_id): JsonResponse
    {
        // Проверка существования пользователя
        throw_unless((Http::get(ServiceRegistry::getAddress('Auth-Microservice') . '/user/' . $user_uuid . '/exists')->json(['data'])), new ModelNotFoundException);

        // Проверка существования роли
        throw_unless($role = $this->roleService->find(['id' => $role_id], -1), new ModelNotFoundException);
        // Проверка наличия роли у пользователя
        throw_unless($this->roleService->hasUserRole($user_uuid, $role->id), new ModelNotFoundException);

        // Удаление роли у пользователя
        $this->roleService->removeRole($user_uuid, $role->id);

        // Возвращение успешного ответа
        return ResponseProvider::render(new ResponseDTO(new ENTITY_UPDATED_Message));
    }

    /**
     * @throws Throwable
     */
    public function admin(Request $request): JsonResponse
    {
        $user_uuid = $request->attributes->get('user_uuid');

        // Проверка существования пользователя
        throw_unless((Http::get(ServiceRegistry::getAddress('Auth-Microservice') . '/user/' . $user_uuid . '/exists')->json(['data'])), new ModelNotFoundException);

        // Проверка существования роли
        throw_unless($role = $this->roleService->find(['name' => 'Administrator'], -1), new ModelNotFoundException);
        // Проверка преждевременного наличия роли у пользователя
        throw_if($this->roleService->hasUserRole($user_uuid, $role->id), new ENTITY_EXISTS_EXCEPTION);

        // Выдача роли пользователю
        $this->roleService->giveRole($user_uuid, $role->id);

        // Возвращение успешного ответа
        return ResponseProvider::render(new ResponseDTO(new ENTITY_UPDATED_Message));
    }
}
