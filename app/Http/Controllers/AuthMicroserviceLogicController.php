<?php

namespace App\Http\Controllers;

use App\Http\Request;
use App\Models\Role;
use App\Services\RoleService;
use Illuminate\Support\Facades\Http;
use phpcommon\Utils\MicroServices;
use phpcommon\Utils\ServiceRegistry;

class AuthMicroserviceLogicController extends Controller
{
    private array $userRequestRules = ['login' => 'required|min:3|max:255', 'password' => 'required|min:8|max:255'];

    private $roleService;

    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    public function register(Request $request)
    {
        $validatedData = $this->validate($request, $this->userRequestRules);

        $response = Http::post(ServiceRegistry::getAddress(MicroServices::AUTH_MICROSERVICE) . '/user', $validatedData);

        if ($response->status() == 201) {
            $defaultRoles = Role::where('is_default', 'true')->get();
            foreach ($defaultRoles as $defaultRole) {
                $this->roleService->giveRole($response->json()['data']['uuid'], $defaultRole->id);
            }
        }

        return response()->json($response->json(), $response->status());
    }
}
