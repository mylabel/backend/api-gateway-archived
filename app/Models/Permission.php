<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Permission extends Model
{

    use HasFactory;

    const accessLevels = [
        'create' => [
            0 => ['*']
        ],
        'get' => [
            0 => ['name'],
            1 => ['name', 'description'],
        ],
        'update' => [
            0 => ['name'],
            1 => ['name', 'description'],
        ],
        'delete' => [
            0 => ['*']
        ]
    ];

    protected $fillable = ['name', 'description'];

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class);
    }
}
