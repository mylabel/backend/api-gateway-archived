<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class UserRole extends Model
{
    protected $table = 'user_role';

    protected $fillable = ['user_uuid', 'role_id'];

    public function roles(): Collection
    {
        return DB::table('roles')->where('id',$this->role_id)->get();
    }
}
