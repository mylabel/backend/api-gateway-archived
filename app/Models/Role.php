<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Role extends Model
{
    use HasFactory;

    const accessLevels = [
        'create' => [
            0 => ['*']
        ],
        'get' => [
            0 => ['name'],
            1 => ['name', 'description', 'access_level', 'is_default'],
        ],
        'update' => [
            0 => ['name'],
            1 => ['name', 'description', 'access_level', 'is_default'],
        ],
        'delete' => [
            0 => ['*']
        ]
    ];

    protected $fillable = ['name', 'description', 'access_level', 'is_default'];

    public function permissions(): BelongsToMany
    {
        return $this->belongsToMany(Permission::class);
    }
}
