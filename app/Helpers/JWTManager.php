<?php

namespace App\Helpers;

class JWTManager
{
    public static function parseJWT($request): bool|string
    {
        // Проверка на соответствие токена
        $token = $request->bearerToken();
        if ($token == null) {
            return false;
        }

        $jwt = JWTManager::parse($token);

        // Проверка токена на валидность
        if (!$jwt) {
            return false;
        }

        // Проверка на истечение времени токена
        if ($jwt->payload->exp < time()) {
            return false;
        }

        // // Проверка совпадений IP адресов
        // if ($jwt->payload->ip_creator != $request->ip()) {
        //     return false;
        // }

        // Проверка соответствия подписей
        if (!JWTManager::checkSignature($jwt)) {
            return false;
        }

        return $jwt->payload->user_uuid;
    }

    public static function parse(string $jwt_base64): JWT|bool
    {
        $jwt_explode = explode('.', $jwt_base64);

        // Если jwt не имеет signature часть
        if (count($jwt_explode) < 3) {
            return false;
        }

        $header = json_decode(base64_decode($jwt_explode[0]));
        $payload = json_decode(base64_decode($jwt_explode[1]));
        $signature = base64_decode($jwt_explode[2]);

        return new JWT($header, $payload, $signature);
    }

    public static function checkSignature(JWT $jwt): bool
    {
        $secret = env('JWT_SECRET');

        return hash_hmac('sha256', json_encode($jwt->header) . json_encode($jwt->payload), $secret) == $jwt->signature;
    }
}
