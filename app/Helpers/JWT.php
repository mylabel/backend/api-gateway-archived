<?php

namespace App\Helpers;

class JWT
{
    public $header;
    public $payload;
    public string $signature;

    public function __construct($header, $payload, $signature)
    {
        $this->header = $header;
        $this->payload = $payload;
        $this->signature = $signature;
    }

    public function base64(): string
    {
        // Сериализация jwt в base_64
        $jwt_header_base64 = base64_encode(json_encode($this->header));
        $jwt_payload_base64 = base64_encode(json_encode($this->payload));
        $jwt_signature_base64 = base64_encode($this->signature);

        return $jwt_header_base64 . '.' . $jwt_payload_base64 . '.' . $jwt_signature_base64;
    }
}
