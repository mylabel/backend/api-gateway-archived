<?php

namespace App\Providers;

use App\Http\Request;
use App\Routing\RouteRegistry;
use App\Services\ServiceRegistry;
use App\Services\ServiceRegistryContract;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * @throws BindingResolutionException
     */
    public function boot()
    {
        $this->app->singleton(RouteRegistry::class, function () {
            return new RouteRegistry();
        });

        $this->app->singleton(Request::class, function () {
            return Request::capture();
        });

        $this->app->bind(ServiceRegistryContract::class, ServiceRegistry::class);

        $this->app->alias(Request::class, 'request');

        $this->registerRoutes();
    }

    /**
     * @throws BindingResolutionException
     */
    protected function registerRoutes()
    {
        $registry = $this->app->make(RouteRegistry::class);

        if ($registry->isEmpty()) {
            return;
        }

        $registry->bind(app());
    }
}
