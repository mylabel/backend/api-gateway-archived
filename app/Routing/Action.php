<?php

namespace App\Routing;


class Action implements ActionContract
{
    protected array $config;

    public function __construct(array $options)
    {
        $this->config = $options;
    }

    public function getPath(): string
    {
        return $this->config['path'];
    }

    public function setPath($url): Action
    {
        $this->config['path'] = $url;

        return $this;
    }

    public function getMethod(): string
    {
        return $this->config['method'];
    }

    public function setMethod($method): Action
    {
        $this->config['method'] = $method;

        return $this;
    }

    public function getAlias(): ?string
    {
        return $this->config['alias'] ?? null;
    }

    public function setAlias($alias): Action
    {
        $this->config['alias'] = $alias;

        return $this;
    }

    public function getService(): string
    {
        return $this->config['service'];
    }

    public function setService(string $service): Action
    {
        $this->config['service'] = $service;

        return $this;
    }

    public function getBodyAsync(): ?array
    {
        return $this->config['body'] ?? null;
    }

    public function setBodyAsync($body): Action
    {
        $this->config['body'] = $body;

        return $this;
    }

    public function getAliases(): string
    {
        return $this->config['aliases'];
    }

    public function addAlias(string $alias): Action
    {
        array_push($this->config['aliases'], $alias);

        return $this;
    }
}
