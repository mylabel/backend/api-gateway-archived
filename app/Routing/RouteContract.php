<?php

namespace App\Routing;

interface RouteContract
{
    public function getID(): string;

    public function getMethod(): string;

    public function getPermission(): ?string;

    public function getDescription(): ?string;

    public function getAuthentication(): ?bool;

    public function getVerification(): ?bool;

    public function getSubscription(): ?bool;

    public function getPath(): string;

    public function getAction(): Action;

    public function setAction(Action $action): Route;

    public function getConfig(): array;
}
