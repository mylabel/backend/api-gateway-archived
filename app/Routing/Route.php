<?php

namespace App\Routing;

class Route implements RouteContract
{
    protected Action $action;

    protected array $config;

    public function __construct(array $options)
    {
        $this->config = $options;
    }

    public function getConfig(): array
    {
        return $this->config;
    }

    public function getID(): string
    {
        return $this->config['id'];
    }

    public function getMethod(): string
    {
        return strtolower($this->config['method']);
    }

    public function getPermission(): ?string
    {
        if (!array_key_exists('permission', $this->config))
            return null;
        return $this->config['permission'];
    }

    public function getDescription(): ?string
    {
        if (!array_key_exists('description', $this->config))
            return null;
        return $this->config['description'];
    }

    public function getAuthentication(): ?bool
    {
        if (!array_key_exists('authentication', $this->config))
            return null;
        return $this->config['authentication'];
    }

    public function getVerification(): ?bool
    {
        if (!array_key_exists('verification', $this->config))
            return null;
        return $this->config['verification'];
    }

    public function getSubscription(): ?bool
    {
        if (!array_key_exists('subscription', $this->config))
            return null;
        return $this->config['subscription'];
    }

    public function getPath(): string
    {
        return $this->config['path'];
    }

    public function setAction(Action $action): Route
    {
        $this->action = $action;

        return $this;
    }

    public function getAction(): Action
    {
        return $this->action;
    }
}
