<?php

namespace App\Routing;

interface ActionContract
{
    public function getPath(): string;

    public function setPath(string $url): Action;

    public function getMethod(): string;

    public function setMethod($method);

    public function getAliases(): string;

    public function addAlias(string $alias): Action;

    public function getService(): string;

    public function setService(string $service): Action;

    public function getBodyAsync(): ?array;

    public function setBodyAsync(array $body): Action;
}
