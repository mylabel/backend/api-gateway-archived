<?php

namespace App\Routing;

use Faker\Provider\Uuid;
use Laravel\Lumen\Application;

class RouteRegistry
{
    protected array $routes = [];

    public function __construct()
    {
        $this->parseConfigRoutes();
    }

    private function parseConfigRoutes(): void
    {
        $configs = collect(glob(__DIR__ . '/../../config/routes/*.json'))->reduce(function ($carry, $item) {
            if ($carry)
                return array_merge($carry, json_decode(file_get_contents($item), true));
            else
                return json_decode(file_get_contents($item), true);
        });

        $this->parseRoutes($configs);
    }

    private function parseRoutes(array $routes): void
    {
        collect($routes)->each(function ($routeDetails) {
            if (!isset($routeDetails['id'])) {
                $routeDetails['id'] = Uuid::uuid();
            }
            $route = (new Route($routeDetails))->setAction(new Action($routeDetails['action']));
            $this->addRoute($route);
        });
    }

    public function addRoute(RouteContract $route)
    {
        $this->routes[] = $route;
    }

    public function isEmpty(): bool
    {
        return empty($this->routes);
    }

    public function getRoutes(): array
    {
        return $this->routes;
    }

    public function getRoute($id): Route
    {
        return collect($this->routes)->first(function (Route $route) use ($id) {
            return $route->getID() == $id;
        });
    }

    public function bind(Application $app)
    {
        collect($this->routes)->each(function (Route $route) use ($app) {
            $method = $route->getMethod();
            $middleware = ['helper:' . $route->getID()];

            if ($verification = $route->getVerification()) {
                $middleware[] = 'authentication:' . true;
                $middleware[] = 'verification:' . $verification;
            } else {
                if ($auth = $route->getAuthentication())
                    $middleware[] = 'authentication:' . $auth;
            }

            if ($permission = $route->getPermission()) {
                $middleware[] = 'authentication:' . true;
                $middleware[] = 'authorization:' . $permission;
            } else {
                if ($auth = $route->getAuthentication())
                    $middleware[] = 'authentication:' . $auth;
            }

            if ($subscription = $route->getSubscription()) {
                $middleware[] = 'authentication:' . true;
                $middleware[] = 'subscription:' . $subscription;
            } else {
                if ($auth = $route->getAuthentication())
                    $middleware[] = 'authentication:' . $auth;
            }

            $app->router->{$method}($route->getPath(), [
                'uses' => 'App\Http\Controllers\GatewayController@' . $method,
                'middleware' => $middleware
            ]);
        });
    }
}
