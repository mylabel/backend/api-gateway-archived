<?php

namespace App\Console\Commands;

use App\Models\Permission;
use App\Models\Role;
use App\Models\RolePermission;
use Illuminate\Console\Command;

class RefreshPermissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permissions:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh permissions in accordance with configuration files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $this->initPermissions();
        $this->initAdmin();
        $this->initUser();
        $this->initModer();
    }

    function initPermissions()
    {
        Permission::firstOrCreate(['name' => 'view-role', 'description' => 'Permission to View a specific role']);
        Permission::firstOrCreate(['name' => 'delete-role', 'description' => 'Permission to Delete role']);
        Permission::firstOrCreate(['name' => 'create-role', 'description' => 'Permission to Create role']);
        Permission::firstOrCreate(['name' => 'view-role-permissions', 'description' => 'Permission to View role permissions']);
        Permission::firstOrCreate(['name' => 'replace-role', 'description' => 'Permission to Replace role']);
        Permission::firstOrCreate(['name' => 'update-role', 'description' => 'Permission to Partially update role']);
        Permission::firstOrCreate(['name' => 'view-roles', 'description' => 'Permission to View roles']);
        Permission::firstOrCreate(['name' => 'view-permission', 'description' => 'Permission to View permission']);
        Permission::firstOrCreate(['name' => 'view-permissions', 'description' => 'Permission to View all permissions']);
        Permission::firstOrCreate(['name' => 'delete-permission', 'description' => 'Permission to Delete permission']);
        Permission::firstOrCreate(['name' => 'create-permission', 'description' => 'Permission to Create permission']);
        Permission::firstOrCreate(['name' => 'replace-permission', 'description' => 'Permission to Completely replace permission']);
        Permission::firstOrCreate(['name' => 'update-permission', 'description' => 'Permission to Partially update permission']);
        Permission::firstOrCreate(['name' => 'view-permission', 'description' => 'Permission to View permissions']);
        Permission::firstOrCreate(['name' => 'remove-permission', 'description' => 'Permission to Issue a role to a user']);
        Permission::firstOrCreate(['name' => 'give-permission', 'description' => 'Permission to Remove a role from a user']);
        Permission::firstOrCreate(['name' => 'view-user-roles', 'description' => 'Permission to View user roles']);
        Permission::firstOrCreate(['name' => 'give-permission', 'description' => 'Permission to Grant permissions to a role']);
        Permission::firstOrCreate(['name' => 'remove-permission', 'description' => 'Permission to Revoke permission from a role']);

        collect(collect(glob(__DIR__ . '/../../../config/routes/*.json'))->reduce(function ($carry, $item) {
            if ($carry)
                return array_merge($carry, json_decode(file_get_contents($item), true));
            else
                return json_decode(file_get_contents($item), true);
        }))->each(function ($route) {
            if (array_key_exists('permission', $route)) {
                Permission::firstOrCreate(['name' => $route['permission'], 'description' => 'Permission to ' . $route['description']]);
            }
        });
    }

    function initAdmin()
    {
        $role = Role::firstOrCreate([
            'name' => 'Administrator',
            'description' => 'Role that possesses all possible permissions',
            'access_level' => -1,
        ]);

        foreach (Permission::all() as $permission) {
            RolePermission::firstOrCreate(['role_id' => $role['id'], 'permission_id' => $permission['id']]);
        }
    }

    function initUser()
    {
        $permissions = [
            'update-user',
            'view-user',
            'view-all-users',
            'view-yourself',
            'upload-user-avatar',
            'replace-user',
            'create-artist',
            'view-artist',
            'view-user-artists',
            'delete-artist',
            'view-product',
            'view-products',
            'create-order',
            'remove-product',
            'delete-order',
            'view-user-orders',
            'view-order',
            'view-order-products',
            'update-order',
            'pay-order',
            'create-release',
            'view-release',
            'view-user-releases',
            'update-release',
            'add-track',
            'publish-release',
            'view-language',
            'view-languages',
            'view-music-style',
            'view-music-styles',
            'view-release-type',
            'view-release-types',
            'view-distributor',
            'view-distributors',
            'view-contributor-role',
            'view-contributor-roles',
            'update-release',
            'generate-cover-preview',
            'view-covers',
            'view-cover',
            'render-cover',
            'generate-cover',
            'update-cover',
            'view-covers-count',
            'view-subscription',
            'view-subscriptions',
            'view-user-covers-remainder',
            'view-user-covers-subscription',
            'view-tickets',
            'view-own-tickets',
            'view-ticket',
            'create-ticket',
            'create-ticket-message',
            'view-ticket-messages',
            'view-ticket-message',
            'read-ticket-messages',
            'view-ticket-image',
            'update-track',
            'update-release',
            'delete-track',
            'delete-release',
            'view-release',
            'view-track',
            'view-faqs',
            'view-faqs',
            'view-social-sources',
            'add-user-social-network',
            'delete-user-social-network',
            'update-user-social-network',
            'add-artist-social-network',
            'delete-artist-social-network',
            'update-artist-social-network'
        ];

        $role = Role::firstOrCreate([
            'name' => 'User',
            'description' => 'Standard Role for users',
            'access_level' => 1,
            'is_default' => true
        ]);

        foreach ($permissions as $permissionName) {
            $permission = Permission::where(['name' => $permissionName])->first();
            if ($permission)
                RolePermission::firstOrCreate(['role_id' => $role['id'], 'permission_id' => $permission['id']]);
        }
    }

    function initModer()
    {
        $permissions = [
            'view-user-roles',
            'view-role-permissions',
            'view-roles',
            'view-artists',
            'view-user-artists',
            'create-product',
            'replace-product',
            'update-product',
            'delete-product',
            'view-orders',
            'deny-release',
            'accept-release',
            'delete-cover',
            'create-subscription',
            'replace-subscription',
            'update-subscription',
            'delete-subscription',
            'check-subscription-status',
            'give-subscription',
            'delete-ticket',
            'update-ticket',
            'update-ticket-message',
            'delete-ticket-message',
            'create-faq',
            'delete-faq',
            'update-faq'
        ];

        $role = Role::firstOrCreate(
            array(
                'name' => 'Moderator',
                'description' => 'Moderator Role',
                'access_level' => 2,
                'is_default' => false
            ),
        );

        foreach ($permissions as $permissionName) {
            $permission = Permission::where(['name' => $permissionName])->first();
            if ($permission)
                RolePermission::firstOrCreate(['role_id' => $role['id'], 'permission_id' => $permission['id']]);
        }
    }
}
