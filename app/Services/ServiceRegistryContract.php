<?php

namespace App\Services;

interface ServiceRegistryContract
{
    public function resolveInstance($serviceId): string;
}
