<?php

namespace App\Services;

use App\Http\Request;
use App\Routing\ActionContract;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;


class RestClient
{
    protected ServiceRegistryContract $services;

    protected array $params = [
        'headers' => [],
        'timeout' => 40
    ];

    public function __construct(ServiceRegistryContract $services, Request $request)
    {
        $this->services = $services;
        $this->injectHeaders($request);

        return response()->json($request->all());
    }

    private function injectHeaders(Request $request)
    {
        // TODO: Возможно, что-то понадобится потом
        $this->setHeaders($request->headers->all());
    }

    public function setHeaders(array $headers)
    {
        $this->params['headers'] = $headers;

        unset(
            $this->params['headers']['host']
        );
        unset(
            $this->params['headers']['content-length']
        );

        if (array_key_exists('content-type', $headers) && str_starts_with($headers['content-type'][0], 'multipart/form-data;'))
            unset($this->params['headers']['content-type']);

    }

    public function setContentType(string $contentType): RestClient
    {
        $this->params['headers']['Content-Type'] = $contentType;

        return $this;
    }

    public function getHeaders(): array
    {
        return $this->params['headers'];
    }

    public function setBody(array $body): RestClient
    {
        $this->params['body'] = $body;
        // $this->setContentSize(strlen(json_encode($body)));

        return $this;
    }

    public function setContentSize($contentSize): static
    {
        $this->params['headers']['content-length'] = $contentSize;

        return $this;
    }

    public function setFiles(array $files): RestClient
    {
        $this->params['timeout'] = 20;
        $this->params['files'] = [];

        collect($files)->each(function ($file, $key) {
            if (is_array($file)) {
                collect($file)->each(function ($subfile) use ($key) {
                    array_push($this->params['files'], [
                        'name' => $key . '[]',
                        'contents' => file_get_contents($subfile->getRealPath()),
                        'filename' => $subfile->getClientOriginalName()
                    ]);
                });
            } else
                array_push($this->params['files'], [
                    'name' => $key,
                    'contents' => file_get_contents($file->getRealPath()),
                    'filename' => $file->getClientOriginalName()
                ]);
        });

        return $this;
    }

    public function setAccessLevel($accessLevel): RestClient
    {
        if ($accessLevel === null)
            $accessLevel = 0;
        $this->params['headers']['access-level'] = $accessLevel;

        return $this;
    }

    public function setUserUUID($userUUID): RestClient
    {
        $this->params['headers']['user-uuid'] = $userUUID;

        return $this;
    }

    public function post(string $url): Response
    {
        $client = Http::withHeaders($this->params['headers']);

        if (array_key_exists('files', $this->params)) {
            foreach ($this->params['files'] as $file) {
                $client = $client->attach(...$file);
            }

            if (array_key_exists('body', $this->params)) {
                $flatten = function ($array, $original_key = '') use (&$flatten) {
                    $output = [];
                    foreach ($array as $key => $value) {
                        $new_key = $original_key;

                        if (empty($original_key)) {
                            $new_key .= $key;
                        } else {
                            $new_key .= '[' . $key . ']';
                        }

                        if (is_array($value)) {
                            $output = array_merge($output, $flatten($value, $new_key));
                        } else {
                            $output[$new_key] = $value;
                        }
                    }

                    return $output;
                };

                if (collect($this->params['body'])->filter(function ($value) {
                    return is_array($value);
                })->count()) {
                    $flat_array = $flatten($this->params['body']);
                    $data = [];

                    foreach ($flat_array as $key => $value) {
                        $data[] = [
                            'name' => $key,
                            'contents' => $value
                        ];
                    }

                    return $client->post($url, $data);
                }
                return $client->post($url, $this->params['body']);
            }
        }

        if (array_key_exists('body', $this->params))
            return $client->post($url, $this->params['body']);
        return $client->post($url);
    }

    public function put(string $url): Response
    {
        return Http::withHeaders($this->params['headers'])
            ->put($url, $this->params['body']);
    }

    public function patch(string $url): Response
    {
        $client = Http::withHeaders($this->params['headers']);

        if (array_key_exists('body', $this->params))
            return $client->patch($url, $this->params['body']);

        return $client->patch($url);
    }

    public function get(string $url): Response
    {
        return Http::withHeaders($this->params['headers'])
            ->get($url);
    }

    public function delete(string $url): Response
    {
        return Http::withHeaders($this->params['headers'])
            ->delete($url);
    }

    public function syncRequest(ActionContract $action, array $parametersJar)
    {
        return $this->{$action->getMethod()}(
            $this->buildUrl($action, $parametersJar)
        );
    }

    private function buildUrl(ActionContract $action, $parametersJar): string
    {
        $url = $this->injectParams($action->getPath(), $parametersJar);
        if ($url[0] != '/') $url = '/' . $url;
        if (isset($parametersJar['query_string'])) $url .= '?' . $parametersJar['query_string'];

        return $this->services->resolveInstance($action->getService()) . $url;
    }

    private function injectParams(string $url, array $params, string $prefix = ''): string
    {
        foreach ($params as $key => $value) {
            if (is_array($value)) {
                $url = $this->injectParams($url, $value, $prefix . $key . '.');
            } else if (is_string($value) || is_numeric($value)) {
                $url = str_replace("{" . $prefix . $key . "}", $value, $url);
            }
        }
        return $url;
    }

}
