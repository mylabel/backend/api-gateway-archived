<?php

namespace App\Services;


class ServiceRegistry implements ServiceRegistryContract
{
    public function resolveInstance($serviceId): string
    {
        return \phpcommon\Utils\ServiceRegistry::getAddress($serviceId);
    }
}
