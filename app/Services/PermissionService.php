<?php

namespace App\Services;


use App\Models\Permission;
use App\Models\Role;
use App\Models\RolePermission;
use JetBrains\PhpStorm\Pure;
use phpcommon\http\BaseService;

class PermissionService extends BaseService
{
    #[Pure] public function __construct()
    {
        parent::__construct(Permission::class);
    }

    public function getRolePermissions($roleID)
    {
        return Role::where('id', $roleID)->first()->permissions;
    }

    public function hasRolePermission($roleID, $permissionID): bool
    {
        foreach ($this->getRolePermissions($roleID) as $permission)
            if ($permission->id == $permissionID)
                return True;

        return False;
    }

    public function givePermission($roleID, $permissionID)
    {
        return RolePermission::create(['role_id' => $roleID, 'permission_id' => $permissionID]);
    }

    public function removePermission($roleID, $permissionID)
    {
        return RolePermission::where(['role_id' => $roleID, 'permission_id' => $permissionID])->first()->delete();
    }
}
