<?php

namespace App\Services;

use App\Models\Role;
use App\Models\UserRole;
use JetBrains\PhpStorm\Pure;
use phpcommon\http\BaseService;


class RoleService extends BaseService
{
    #[Pure] public function __construct()
    {
        parent::__construct(Role::class);
    }

    public function getUserRoles($userUUID)
    {
        return Role::find(UserRole::where('user_uuid', $userUUID)->get('role_id'));
    }

    public function hasUserRole($userUUID, $roleID): bool
    {
        foreach (UserRole::where('role_id', $roleID)->get('user_uuid') as $userRole)
            if ($userUUID == $userRole->user_uuid)
                return True;

        return False;
    }

    public function giveRole($userUUID, $roleID)
    {
        return UserRole::create(['user_uuid' => $userUUID, 'role_id' => $roleID]);
    }

    public function removeRole($userUUID, $roleID)
    {
        return UserRole::where(['user_uuid' => $userUUID, 'role_id' => $roleID])->first()->delete();
    }
}
