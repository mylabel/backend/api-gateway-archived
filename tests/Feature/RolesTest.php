<?php

namespace Feature;

use App\Helpers\JWT;
use App\Models\Permission;
use App\Models\Role;
use App\Models\RolePermission;
use App\Models\UserRole;
use Illuminate\Support\Str;
use Laravel\Lumen\Testing\DatabaseMigrations;
use TestCase;


class RolesTest extends TestCase
{

    use DatabaseMigrations;

    //тест на получение всех ролей имея токен доступа и права
    public function testGetSuccessRoles()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        //получаю id разрешения
        $permision = Permission::firstWhere('name', 'view-roles');

        //добавляю разрешение для роли
        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permision->id]);

        $this->get('/roles', ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(200, $this->response->status());
    }

    //тест на попытку получения ролей без прав доступа

    private function createAccesssToken(string $uuid): JWT|bool
    {
        $jwt_payload = [
            'name' => 'user',
            'email' => 'user@mail.ru',
            'user_uuid' => $uuid,
            'ip_creator' => '127.0.0.1',
        ];

        $jwt_life_time = env('ACCESS_TOKEN_LIFE_TIME', 15) * 60;

        if (!$jwt_payload) {
            return false;
        }

        $jwt_header = ['alg' => 'HS256', 'typ' => 'JWT'];
        $jwt_payload += ['exp' => time() + $jwt_life_time];

        $secret = env('JWT_SECRET');

        // Генерация подписи jwt
        $jwt_signature = hash_hmac('sha256', json_encode($jwt_header) . json_encode($jwt_payload), $secret);

        return new JWT($jwt_header, $jwt_payload, $jwt_signature);
    }

    //тест на попытку получения ролей без токена

    public function testGetRolesWithoutPermission()
    {
        //view-roles
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        $this->get('/roles', ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(401, $this->response->status());
    }

    public function testWithoutTokenGetRoles()
    {
        $this->get('/roles');
        $this->assertEquals(401, $this->response->status());
    }

    public function testGetSuccessRole()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        //получаю id разрешения
        $permision = Permission::firstWhere('name', 'view-role');

        //добавляю разрешение для роли
        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permision->id]);

        $this->get('/role/1', ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(200, $this->response->status());
    }

    public function testGetUnknownRole()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        //получаю id разрешения
        $permision = Permission::firstWhere('name', 'view-role');

        //добавляю разрешение для роли
        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permision->id]);

        $this->get('/role/999', ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(404, $this->response->status());
    }

    public function testGetRoleWithoutPermissionRole()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        $this->get('/role/1', ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(401, $this->response->status());
    }

    public function testDeleteRole()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        $delete_role = Role::create(['name' => 'delete', 'description' => 'delete', 'access_level' => 0]);
        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        //получаю id разрешения
        $permision = Permission::firstWhere('name', 'delete-role');

        //добавляю разрешение для роли
        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permision->id]);

        $this->delete('/role/' . $delete_role->id, array(), ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(200, $this->response->status());
    }

    public function testDeleteUnknownRole()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        //получаю id разрешения
        $permision = Permission::firstWhere('name', 'delete-role');

        //добавляю разрешение для роли
        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permision->id]);

        $this->delete('/role/999', array(), ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(404, $this->response->status());
    }

    public function testDeleteWithoutRole()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        $this->delete('/role/999', array(), ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(401, $this->response->status());
    }

    public function testDeleteWithoutTokenRole()
    {
        $this->delete('/role/999');

        $this->assertEquals(401, $this->response->status());
    }

    public function testCreateRole()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        //получаю id разрешения
        $permision = Permission::firstWhere('name', 'create-role');

        //добавляю разрешение для роли
        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permision->id]);

        $this->post('/role', ['name' => 'test', 'description' => 'json', 'access_level' => 0], ['Authorization' => 'Bearer ' . $token->base64()]);
        $this->assertEquals(201, $this->response->status());
    }

    public function testCreateRoleWithoutPermission()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        $this->post('/role', ['name' => 'post_role', 'description' => 'role_for_test', 'access_level' => 3], ['Content-Type' => 'application/json', 'Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(401, $this->response->status());
    }

    public function testCreateRoleWithoutToken()
    {
        $this->post('/role', ['name' => 'post_role', 'description' => 'role_for_test', 'access_level' => 3], ['Content-Type' => 'application/json']);

        $this->assertEquals(401, $this->response->status());
    }

    public function testGetRolePermissions()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        //получаю id разрешения
        $permision = Permission::firstWhere('name', 'view-role-permissions');

        //добавляю разрешение для роли
        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permision->id]);

        $this->get('/role/1/permissions', ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(200, $this->response->status());
    }

    public function testGetRolePermissionsWithoutPermission()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        $this->get('/role/1/permissions', ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(401, $this->response->status());
    }

    public function testGetRolePermissionsWithoutToken()
    {
        $this->get('/role/1/permissions');

        $this->assertEquals(401, $this->response->status());
    }

    public function testReplaceRole()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        //получаю id разрешения
        $permision = Permission::firstWhere('name', 'replace-role');

        //добавляю разрешение для роли
        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permision->id]);

        $this->put('/role/2', ['name' => 'replace_role', 'description' => 'replace_desc', 'access_level' => 0], ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(200, $this->response->status());
    }

    public function testReplaceRoleWithoutPermission()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $this->put('/role/2', ['name' => 'replace_role', 'description' => 'replace_desc', 'access_level' => 0], ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(401, $this->response->status());
    }

    public function testReplaceRoleWithoutToken()
    {
        $this->put('/role/2', ['name' => 'replace_role', 'description' => 'replace_desc', 'access_level' => 0]);

        $this->assertEquals(401, $this->response->status());
    }

    public function testUpdateRole()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        //получаю id разрешения
        $permision = Permission::firstWhere('name', 'update-role');

        //добавляю разрешение для роли
        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permision->id]);

        $this->patch('/role/2', ['description' => 'replace_desc'], ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(200, $this->response->status());
    }

    public function testWrongUpdateRole()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        //получаю id разрешения
        $permision = Permission::firstWhere('name', 'update-role');

        //добавляю разрешение для роли
        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permision->id]);

        $this->patch('/role/2', ['description' => 'replace_desc'], ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(200, $this->response->status());
    }

    public function testUpdateRoleWithoutPermission()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $this->patch('/role/2', ['description' => 'replace_desc'], ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(401, $this->response->status());
    }

    // вспомогательный метод для создания токенов ( небольщая замена auth.mic )

    public function testUpdateRoleWithoutToken()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $this->patch('/role/2', ['description' => 'replace_desc']);

        $this->assertEquals(401, $this->response->status());
    }

}
