<?php

namespace Feature;

use App\Helpers\JWT;
use App\Models\Permission;
use App\Models\Role;
use App\Models\RolePermission;
use App\Models\UserRole;
use Illuminate\Support\Str;
use Laravel\Lumen\Testing\DatabaseMigrations;
use TestCase;


class PermissionTest extends TestCase
{
    use DatabaseMigrations;

    public function testGetPermission()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        //получаю id разрешения
        $permission = Permission::firstWhere('name', 'view-permission');

        //добавляю разрешение для роли
        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permission->id]);

        $this->get('/permission/1', ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(200, $this->response->status());
    }

    private function createAccesssToken(string $uuid): JWT|bool
    {
        $jwt_payload = [
            'name' => 'user',
            'email' => 'user@mail.ru',
            'user_uuid' => $uuid,
            'ip_creator' => '127.0.0.1',
        ];

        $jwt_life_time = env('ACCESS_TOKEN_LIFE_TIME', 15) * 60;

        if (!$jwt_payload) {
            return false;
        }

        $jwt_header = ['alg' => 'HS256', 'typ' => 'JWT'];
        $jwt_payload += ['exp' => time() + $jwt_life_time];

        $secret = env('JWT_SECRET');

        // Генерация подписи jwt
        $jwt_signature = hash_hmac('sha256', json_encode($jwt_header) . json_encode($jwt_payload), $secret);

        return new JWT($jwt_header, $jwt_payload, $jwt_signature);
    }

    public function testGetPermissionWithoutPermission()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        $this->get('/permission/1', ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(401, $this->response->status());
    }

    public function testGetPermissionWithoutToken()
    {
        $this->get('/permission/1');

        $this->assertEquals(401, $this->response->status());
    }

    public function testDeletePermission()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        //получаю id разрешения
        $permission = Permission::firstWhere('name', 'delete-permission');

        //добавляю разрешение для роли
        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permission->id]);

        $this->delete('/permission/1', array(), ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(200, $this->response->status());
    }

    public function testDeletePermissionWithoutPermission()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        $this->delete('/permission/1', array(), ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(401, $this->response->status());
    }

    public function testDeletePermissionWithoutToken()
    {
        $this->delete('/permission/1', array());

        $this->assertEquals(401, $this->response->status());
    }

    public function testCreatePermission()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        $permission = Permission::where('name', 'create-permission')->first();

        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permission->id]);

        $this->post('/permission', ['name' => 'testpermision', 'description' => 'testdesc'], ['Authorization' => 'Bearer ' . $token->base64()]);
        $this->assertEquals(201, $this->response->status());
    }

    public function testWrongCreatePermission()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        $permission = Permission::where('name', 'create-permission')->first();

        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permission->id]);

        $this->post('/permission', ['description' => 'testdesc'], ['Authorization' => 'Bearer ' . $token->base64()]);
        $this->assertEquals(422, $this->response->status());
    }

    public function testCreatePermissionWithoutPermission()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        $this->post('/permission', ['name' => 'testpermision', 'description' => 'testdesc'], ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(401, $this->response->status());
    }

    public function testCreatePermissionWithoutToken()
    {
        $this->post('/permission', ['name' => 'testpermision', 'description' => 'testdesc']);

        $this->assertEquals(401, $this->response->status());
    }

    public function testReplacePermission()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        $permission = Permission::where('name', 'replace-permission')->first();

        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permission->id]);

        $this->put('/permission/1', ['name' => 'update_permision', 'description' => 'upfate_desc'], ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(200, $this->response->status());
    }

    public function testWrongReplacePermission()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        $permission = Permission::where('name', 'replace-permission')->first();

        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permission->id]);

        $this->put('/permission/1', [Str::random(10) => 'replace-permission', Str::random(10) => 'replace-permission-desc'], ['Authorization' => 'Bearer ' . $token->base64()]);
        $this->assertEquals(422, $this->response->status());
    }

    public function testReplacePermissionWithoutPermission()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        $this->put('/permission/1', ['name' => 'replace-permission', 'description' => 'replace-permission-desc'], ['Authorization' => 'Bearer ' . $token->base64()]);
        $this->assertEquals(401, $this->response->status());
    }

    public function testReplacePermissionWithoutToken()
    {
        $this->put('/permission/1', ['name' => 'replace-permission', 'description' => 'replace-permission-desc']);
        $this->assertEquals(401, $this->response->status());
    }

    public function testUpdatePermission()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        $permission = Permission::where('name', 'update-permission')->first();

        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permission->id]);

        $this->patch('/permission/1', ['name' => 'update-permission', 'description' => 'update_desc'], ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(200, $this->response->status());
    }

    public function testUpdatePermissionWithoutPermission()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        $this->patch('/permission/1', ['name' => 'update-permission', 'description' => 'update_desc'], ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(401, $this->response->status());
    }

    ####

    public function testUpdatePermissionWithoutToken()
    {
        $this->patch('/permission/1', ['name' => 'update-permission', 'description' => 'update_desc']);

        $this->assertEquals(401, $this->response->status());
    }

    public function testGetPermissions()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        //получаю id разрешения
        $permission = Permission::firstWhere('name', 'view-permissions');

        //добавляю разрешение для роли
        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permission->id]);

        $this->get('/permissions', ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(200, $this->response->status());
    }

    public function testGetPermissionsWithoutPermission()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        $this->get('/permissions', ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(401, $this->response->status());
    }

    public function testGetPermissionsWithoutToken()
    {
        $this->get('/permissions');

        $this->assertEquals(401, $this->response->status());
    }

    public function testGivePermissionRole()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        //получаю id разрешения
        $permission = Permission::firstWhere('name', 'give-permission');

        //добавляю разрешение для роли
        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permission->id]);

        $this->get('/role/' . $role->id . '/permission/1', ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(200, $this->response->status());
    }

    public function testGiveUnknownPermissionRole()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        //получаю id разрешения
        $permission = Permission::firstWhere('name', 'give-permission');

        //добавляю разрешение для роли
        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permission->id]);

        $this->get('/role/' . $role->id . '/permission/9999', ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(404, $this->response->status());
    }

    public function testGivePermissionRoleWithoutPermission()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        $this->get('/role/' . $role->id . '/permission/1', ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(401, $this->response->status());
    }

//

    public function testGivePermissionRoleWithoutToken()
    {
        $role = Role::factory()->create();

        $this->get('/role/' . $role->id . '/permission/1');

        $this->assertEquals(401, $this->response->status());
    }

    public function testDeletePermissionRole()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        //получаю id разрешения
        $permission = Permission::firstWhere('name', 'remove-permission');

        //добавляю разрешение для роли
        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permission->id]);
        RolePermission::create(['role_id' => $role->id, 'permission_id' => 1]);

        $this->delete('/role/' . $role->id . '/permission/1', array(), ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(200, $this->response->status());
    }

    public function testDeleteUnknownPermissionRole()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        //получаю id разрешения
        $permission = Permission::firstWhere('name', 'remove-permission');

        //добавляю разрешение для роли
        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permission->id]);
        RolePermission::create(['role_id' => $role->id, 'permission_id' => 1]);

        $this->delete('/role/' . $role->id . '/permission/9999', array(), ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(404, $this->response->status());
    }

    public function testDeletePermissionRoleWithoutPermission()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccesssToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);
        RolePermission::create(['role_id' => $role->id, 'permission_id' => 1]);

        $this->delete('/role/' . $role->id . '/permission/1', array(), ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(401, $this->response->status());
    }

    // вспомогательный метод для создания токенов (небольшая замена auth.mic)

    public function testDeletePermissionRoleWithoutToken()
    {
        $role = Role::factory()->create();

        $this->delete('/role/' . $role->id . '/permission/1');

        $this->assertEquals(401, $this->response->status());
    }

}
