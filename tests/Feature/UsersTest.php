<?php

namespace Feature;

use App\Helpers\JWT;
use App\Models\Permission;
use App\Models\Role;
use App\Models\RolePermission;
use App\Models\UserRole;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Laravel\Lumen\Testing\DatabaseMigrations;
use TestCase;

class UsersTest extends TestCase
{

    use DatabaseMigrations;


    public function testGetUserRoles()
    {

        $user_uuid = Str::uuid();

        $token = $this->createAccessToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        //получаю id разрешения
        $permission = Permission::firstWhere('name', 'view-user-roles');

        //добавляю разрешение для роли
        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permission->id]);

        $this->get('/user/' . $user_uuid . '/roles', ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(200, $this->response->status());
    }

    private function createAccessToken(string $uuid): JWT|bool
    {
        $jwt_payload = [
            'name' => 'user',
            'email' => 'user@mail.ru',
            'user_uuid' => $uuid,
            'ip_creator' => '127.0.0.1',
        ];

        $jwt_life_time = env('ACCESS_TOKEN_LIFE_TIME', 15) * 60;

        if (!$jwt_payload) {
            return false;
        }

        $jwt_header = ['alg' => 'HS256', 'typ' => 'JWT'];
        $jwt_payload += ['exp' => time() + $jwt_life_time];

        $secret = env('JWT_SECRET');

        // Генерация подписи jwt
        $jwt_signature = hash_hmac('sha256', json_encode($jwt_header) . json_encode($jwt_payload), $secret);

        return new JWT($jwt_header, $jwt_payload, $jwt_signature);
    }

    public function testGetUserRolesWithoutPermissions()
    {

        $user_uuid = Str::uuid();

        $token = $this->createAccessToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        $this->get('/user/' . $user_uuid . '/roles', ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(401, $this->response->status());
    }


    //

    public function testGetUserRolesWithoutToken()
    {
        $this->get('/user/' . Str::uuid() . '/roles');

        $this->assertEquals(401, $this->response->status());
    }

    public function testCreateUserRole()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccessToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        //получаю id разрешения
        $permission = Permission::firstWhere('name', 'give-role');

        //добавляю разрешение для роли
        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permission->id]);

        Http::fake(['*' => Http::response(['data' => true])]);
        $this->post('/user/' . $user_uuid . '/role/' . $role->id - 1, array(), ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(200, $this->response->status());
    }

    public function testCreateUserRoleWithoutPermission()
    {

        $user_uuid = Str::uuid();

        $token = $this->createAccessToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        $this->post('/user/' . $user_uuid . '/role/' . $role->id, array(), ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(401, $this->response->status());
    }

    public function testCreateUserRoleWithoutToken()
    {
        $this->post('/user/' . Str::uuid() . '/role/' . 1);

        $this->assertEquals(401, $this->response->status());
    }

    public function testGetUserRoleWithoutPermissions()
    {

        $user_uuid = Str::uuid();

        $token = $this->createAccessToken($user_uuid);

        $role = Role::factory()->create();

        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);

        $this->post('/user/' . $user_uuid . '/role/' . $role->id, ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(401, $this->response->status());
    }

    public function testGetUserRoleWithoutToken()
    {
        $this->post('/user/' . Str::uuid() . '/role/' . '1');

        $this->assertEquals(401, $this->response->status());
    }

    public function testDeleteUserRole()
    {

        $user_uuid = Str::uuid();

        $token = $this->createAccessToken($user_uuid);

        $role = Role::factory()->create();

        $delete_role = Role::factory()->create();
        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $delete_role->id]);

        //получаю id разрешения
        $permission = Permission::firstWhere('name', 'remove-role');

        //добавляю разрешение для роли
        RolePermission::create(['role_id' => $role->id, 'permission_id' => $permission->id]);

        Http::fake(['*' => Http::response(['data' => true])]);
        $this->delete('/user/' . $user_uuid . '/role/' . $delete_role->id, array(), ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(200, $this->response->status());
    }

    public function testDeleteUserRoleWithoutPermission()
    {
        $user_uuid = Str::uuid();

        $token = $this->createAccessToken($user_uuid);

        $role = Role::factory()->create();

        $delete_role = Role::factory()->create();
        //добавляю пользователя в роль 'test'
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $role->id]);
        UserRole::create(['user_uuid' => $user_uuid, 'role_id' => $delete_role->id]);

        $this->delete('/user/' . $user_uuid . '/role/' . $delete_role->id, array(), ['Authorization' => 'Bearer ' . $token->base64()]);

        $this->assertEquals(401, $this->response->status());
    }

    // вспомогательный метод для создания токенов ( небольщая замена auth.mic )

    public function testDeleteUserRoleWithoutToken()
    {
        $delete_role = Role::factory()->create();
        UserRole::create(['user_uuid' => Str::uuid(), 'role_id' => $delete_role->id]);

        $this->delete('/user/' . Str::uuid() . '/role/' . $delete_role->id, array());

        $this->assertEquals(401, $this->response->status());
    }
}
