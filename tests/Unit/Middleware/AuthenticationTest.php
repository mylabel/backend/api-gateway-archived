<?php

namespace Unit\Middleware;

use App\Http\Middleware\AuthenticationMiddleware;
use App\Http\Request;
use Illuminate\Validation\UnauthorizedException;
use TestCase;
use Throwable;


class AuthenticationTest extends TestCase
{
    /**
     * @throws Throwable
     */
    public function testAuthenticate()
    {

        $token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX3V1aWQiOiIxMjNlNDU2Ny1lODliLTEyZDMtYTQ1Ni00MjY2NTU0NDAwMDAiLCJpcF9jcmVhdG9yIjoiMTcyLjIwLjAuMSIsImVtYWlsIjoiZW1haWwjbWFpbC5ydSIsIm5hbWUiOiJ2bGFkIiwiZXhwIjoyMTQ3NzQ4MzAwMH0=.NmM5NzExYjQ2ODMwZDQ2NzkwYzMyZDQ0MDIwZGQ5NTQzZDQyMjk4MDE0ODZjNmUxMDZhMzg1OWVjMmQzNzE5NQ==';
        $request = new Request();
        $request->headers->add(['Authorization' => 'Bearer ' . $token]);
        $this->assertEquals(null, (new AuthenticationMiddleware())->handle(new Request(), function () {
        }, false));
    }

    /**
     * @throws Throwable
     */
    public function testWrongJwtExcept()
    {

        $this->expectException(UnauthorizedException::class);

        (new AuthenticationMiddleware())->handle(new Request(), function () {
        }, 'test-permission');
    }
}
