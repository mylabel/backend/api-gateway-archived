<?php

namespace Unit\Middleware;

use App\Http\Middleware\AuthorizationMiddleware;
use App\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use TestCase;
use Throwable;


class AuthorizationTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @throws Throwable
     */
    public function testNonExistentRoleExceptAuth()
    {

        $this->expectException(BadRequestException::class);

        (new AuthorizationMiddleware())->handle(new Request(), function () {
        }, 'test-permission');
    }

    /**
     * @throws Throwable
     */
    public function testExceptAuth()
    {

        Permission::create(['name' => 'test-permission', 'description' => 'test-description', 'access_level' => 0]);

        $this->expectException(UnauthorizedException::class);

        (new AuthorizationMiddleware())->handle(new Request(), function () {
        }, 'test-permission');
    }
}
