<?php

namespace Unit\Controllers;

use App\Http\Controllers\RoleController;
use App\Models\Role;
use App\Services\RoleService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Mockery;
use phpcommon\Handler\Exceptions\ENTITY_EXISTS_EXCEPTION;
use TestCase;
use Throwable;

class RoleControllerTest extends TestCase
{
    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testGetItem()
    {
        $role = Role::factory()->make();

        $roleService = Mockery::mock(RoleService::class);

        $roleService->shouldReceive('find')->andReturn($role);

        $roleController = new RoleController($roleService);

        $this->assertEquals(200, $roleController->item($role->id)->status());

    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testGetUnknownItem()
    {

        $this->expectException(ModelNotFoundException::class);

        $roleService = Mockery::mock(RoleService::class);

        $roleService->shouldReceive('find')->andReturn(Null);

        $roleController = new RoleController($roleService);

        $roleController->item(999);

    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testDeleteItem()
    {

        $role = Role::factory()->make();

        $roleService = Mockery::mock(RoleService::class);

        $roleService->shouldReceive('destroy')->andReturn(true);

        $roleController = new RoleController($roleService);

        $this->assertEquals(200, $roleController->delete($role->id)->status());

    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testDeleteUnknownItem()
    {

        $this->expectException(ModelNotFoundException::class);

        $roleService = Mockery::mock(RoleService::class);

        $roleService->shouldReceive('find')->andReturn(Null);

        $roleController = new RoleController($roleService);

        $roleController->item(999);

    }

    /**
     * @throws Throwable
     */
    public function testGetItems()
    {

        $role = Role::factory()->make();

        $roleService = Mockery::mock(RoleService::class);

        $roleService->shouldReceive('all')->andReturn($role);

        $roleController = new RoleController($roleService);

        $this->assertEquals(200, $roleController->all()->status());

    }

    /**
     * @throws Throwable
     */
    public function testGetUserRoles()
    {

        $role = Role::factory()->make();

        $roleService = Mockery::mock(RoleService::class);

        $roleService->shouldReceive('getUserRoles')->andReturn($role);

        $roleController = new RoleController($roleService);

        $this->assertEquals(200, $roleController->list(Str::uuid())->status());

    }

    /**
     * @throws ValidationException
     */
    public function testCreateRole()
    {
        $role = Role::factory()->make();

        $roleService = Mockery::mock(RoleService::class);

        $roleService->shouldReceive('create')->andReturn($role);

        $request = new Request();

        $request->merge([
            'name' => $role->name,
            'description' => $role->description,
            'access_level' => $role->access_level
        ]);

        $roleController = new RoleController($roleService);

        $this->assertEquals(201, $roleController->create($request)->status());
    }

    public function testCreateEmptyName()
    {
        $this->expectException(ValidationException::class);

        $role = Role::factory()->make();
        $roleService = Mockery::mock(RoleService::class);
        $roleController = new RoleController($roleService);

        $roleService->shouldReceive('create')->andReturn($role);

        $request = new Request();

        $request->merge([
            'description' => $role->description,
            'access_level' => $role->access_level
        ]);

        $roleController->create($request);
    }

    public function testCreateEmptyAccessLevel()
    {
        $this->expectException(ValidationException::class);

        $role = Role::factory()->make();
        $roleService = Mockery::mock(RoleService::class);
        $roleController = new RoleController($roleService);

        $roleService->shouldReceive('create')->andReturn($role);

        $request = new Request();

        $request->merge([
            'name' => $role->name,
            'description' => $role->description
        ]);

        $roleController->create($request);
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testUpdateRole()
    {

        $role = Role::factory()->make();

        $roleService = Mockery::mock(RoleService::class);

        $roleService->shouldReceive('update')->andReturn($role);

        $request = new Request();

        $request->merge([
            'description' => $role->description
        ]);

        $roleController = new RoleController($roleService);

        $this->assertEquals(200, $roleController->update($request, $role->id . '')->status());
    }

    /**
     * @throws Throwable
     */
    public function testUpdateWrongRole()
    {
        $this->expectException(ValidationException::class);

        $role = Role::factory()->make();

        $roleService = Mockery::mock(RoleService::class);

        $roleService->shouldReceive('update')->andReturn($role);

        $request = new Request();

        $request->merge([
            'description' => str_repeat('description', 50)
        ]);

        $roleController = new RoleController($roleService);

        $this->assertEquals(200, $roleController->update($request, $role->id . '')->status());
    }


    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testUpdateUnknownRole()
    {
        $this->expectException(ModelNotFoundException::class);

        $role = Role::factory()->make();

        $roleService = Mockery::mock(RoleService::class);

        $roleService->shouldReceive('update')->andReturn(Null);

        $request = new Request();

        $request->merge([
            'description' => 'desc'
        ]);

        $roleController = new RoleController($roleService);

        $this->assertEquals(200, $roleController->update($request, $role->id . '')->status());
    }

    //

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testReplaceRole()
    {
        $role = Role::factory()->make();

        $roleService = Mockery::mock(RoleService::class);

        $roleService->shouldReceive('update')->andReturn($role);

        $request = new Request();

        $request->merge([
            'name' => $role->name,
            'description' => $role->description,
            'access_level' => $role->access_level
        ]);

        $roleController = new RoleController($roleService);

        $this->assertEquals(200, $roleController->replace($request, $role->id . '')->status());
    }


    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testUnknownReplaceRole()
    {
        $this->expectException(ModelNotFoundException::class);

        $role = Role::factory()->make();
        $roleService = Mockery::mock(RoleService::class);
        $roleController = new RoleController($roleService);

        $roleService->shouldReceive('update')->andReturn(Null);

        $request = new Request();

        $request->merge([
            'name' => $role->name,
            'description' => $role->description,
            'access_level' => $role->access_level
        ]);

        $roleController->replace($request, $role->id . '');
    }

    /**
     * @throws Throwable
     */
    public function testReplaceEmptyName()
    {
        $this->expectException(ValidationException::class);

        $role = Role::factory()->make();
        $roleService = Mockery::mock(RoleService::class);
        $roleController = new RoleController($roleService);

        $roleService->shouldReceive('update')->andReturn($role);

        $request = new Request();

        $request->merge([
            'description' => $role->description,
            'access_level' => $role->access_level
        ]);

        $roleController->replace($request, $role->id . '');
    }

    /**
     * @throws Throwable
     */
    public function testReplaceEmptyAccessLevel()
    {
        $this->expectException(ValidationException::class);

        $role = Role::factory()->make();
        $roleService = Mockery::mock(RoleService::class);
        $roleController = new RoleController($roleService);

        $roleService->shouldReceive('create')->andReturn($role);

        $request = new Request();

        $request->merge([
            'name' => $role->name,
            'description' => $role->description
        ]);

        $roleController->replace($request, $role->id . '');
    }

    /**
     * @throws Throwable
     */
    public function testGiveRole()
    {
        $role = Role::factory()->make();

        $roleService = Mockery::mock(RoleService::class);

        $roleService->shouldReceive('find')->andReturn($role);

        $roleService->shouldReceive('hasUserRole')->andReturn(false);

        $roleService->shouldReceive('giveRole')->andReturn($role);

        $roleController = new RoleController($roleService);

        Http::fake(['*' => Http::response(['data' => true])]);
        $this->assertEquals(200, $roleController->give(Str::uuid(), $role->id)->status());
    }

    /**
     * @throws Throwable
     */
    public function testGiveExistRole()
    {
        $this->expectException(ENTITY_EXISTS_EXCEPTION::class);

        $role = Role::factory()->make();

        $roleService = Mockery::mock(RoleService::class);

        $roleService->shouldReceive('find')->andReturn($role);

        $roleService->shouldReceive('hasUserRole')->andReturn(true);

        $roleService->shouldReceive('giveRole')->andReturn($role);

        $roleController = new RoleController($roleService);

        Http::fake(['*' => Http::response(['data' => true])]);
        $roleController->give(Str::uuid(), $role->id);
    }

    /**
     * @throws Throwable
     */
    public function testGiveUnknownRole()
    {

        $this->expectException(ModelNotFoundException::class);

        $role = Role::factory()->make();

        $roleService = Mockery::mock(RoleService::class);

        $roleService->shouldReceive('find')->andReturn(null);

        $roleService->shouldReceive('hasUserRole')->andReturn(false);

        $roleService->shouldReceive('giveRole')->andReturn($role);

        $request = new Request();

        $roleController = new RoleController($roleService);
        Http::fake(['*' => Http::response(['data' => true])]);

        $roleController->give($request, Str::uuid());
    }


    /**
     * @throws Throwable
     */
    public function testRemoveRole()
    {
        $role = Role::factory()->make();

        $roleService = Mockery::mock(RoleService::class);

        $roleService->shouldReceive('find')->andReturn($role);

        $roleService->shouldReceive('hasUserRole')->andReturn(true);

        $roleService->shouldReceive('removeRole')->andReturn($role);

        $roleController = new RoleController($roleService);

        Http::fake(['*' => Http::response(['data' => true])]);
        $this->assertEquals(200, $roleController->remove(Str::uuid(), $role->id)->status());
    }

    /**
     * @throws Throwable
     */
    public function testRemoveUnknownRole()
    {

        $this->expectException(ModelNotFoundException::class);

        $role = Role::factory()->make();

        $roleService = Mockery::mock(RoleService::class);

        $roleService->shouldReceive('find')->andReturn(Null);

        $roleService->shouldReceive('hasUserRole')->andReturn(false);

        $roleService->shouldReceive('removeRole')->andReturn($role);

        $request = new Request();

        $roleController = new RoleController($roleService);
        Http::fake(['*' => Http::response(['data' => true])]);

        $roleController->remove($request, Str::uuid());
    }

    /**
     * @throws Throwable
     */
    public function testRemoveUnknownNRole()
    {

        $this->expectException(ModelNotFoundException::class);

        $role = Role::factory()->make();

        $roleService = Mockery::mock(RoleService::class);

        $roleService->shouldReceive('find')->andReturn(null);

        $roleService->shouldReceive('hasUserRole')->andReturn(true);

        $roleService->shouldReceive('removeRole')->andReturn($role);

        $request = new Request();

        $roleController = new RoleController($roleService);
        Http::fake(['*' => Http::response(['data' => true])]);

        $roleController->remove($request, Str::uuid());
    }
}
