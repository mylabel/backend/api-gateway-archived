<?php

namespace Unit\Controllers;

use App\Http\Controllers\PermissionController;
use App\Models\Permission;
use App\Models\Role;
use App\Services\PermissionService;
use App\Services\RoleService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Mockery;
use phpcommon\Handler\Exceptions\ENTITY_EXISTS_EXCEPTION;
use TestCase;
use Throwable;

class PermissionControllerTest extends TestCase
{
    /**
     * @throws Throwable
     */
    public function testGetItems()
    {
        $permission = Permission::factory()->make();

        $permissionService = Mockery::mock(PermissionService::class);
        $roleService = Mockery::mock(RoleService::class);

        $permissionService->shouldReceive('all')->andReturn($permission);

        $permissionController = new  PermissionController($permissionService, $roleService);

        $this->assertEquals(200, $permissionController->all()->status());
    }

    /**
     * @throws Throwable
     */
    public function testGetItemsList()
    {
        $role = Role::factory()->make();

        $permissionService = Mockery::mock(PermissionService::class);
        $roleService = Mockery::mock(RoleService::class);

        $permissionService->shouldReceive('getRolePermissions')->andReturn($role);

        $permissionController = new  PermissionController($permissionService, $roleService);

        $this->assertEquals(200, $permissionController->list($role->id)->status());
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testGetItem()
    {
        $permission = Permission::factory()->make();

        $permissionService = Mockery::mock(PermissionService::class);
        $roleService = Mockery::mock(RoleService::class);

        $permissionService->shouldReceive('find')->andReturn($permission);

        $permissionController = new  PermissionController($permissionService, $roleService);

        $this->assertEquals(200, $permissionController->item(1)->status());
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testExceptGetItem()
    {
        $this->expectException(ModelNotFoundException::class);

        $permissionService = Mockery::mock(PermissionService::class);
        $roleService = Mockery::mock(RoleService::class);

        $permissionService->shouldReceive('find')->andReturn(Null);

        $permissionController = new  PermissionController($permissionService, $roleService);

        $permissionController->item(1);
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testDeleteItem()
    {
        $permissionService = Mockery::mock(PermissionService::class);
        $roleService = Mockery::mock(RoleService::class);

        $permissionService->shouldReceive('destroy')->andReturn(True);

        $permissionController = new  PermissionController($permissionService, $roleService);

        $this->assertEquals(200, $permissionController->delete(1)->status());
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testExceptDeleteItem()
    {
        $this->expectException(ModelNotFoundException::class);

        $permissionService = Mockery::mock(PermissionService::class);
        $roleService = Mockery::mock(RoleService::class);

        $permissionService->shouldReceive('destroy')->andReturn(Null);

        $permissionController = new  PermissionController($permissionService, $roleService);

        $permissionController->delete(1);
    }

    /**
     * @throws ValidationException
     */
    public function testCreatePermission()
    {
        $permission = Permission::factory()->make();

        $permissionService = Mockery::mock(PermissionService::class);
        $roleService = Mockery::mock(RoleService::class);

        $permissionService->shouldReceive('create')->andReturn($permission);

        $permissionController = new  PermissionController($permissionService, $roleService);

        $request = new Request();

        $request->merge([
            'name' => $permission->name,
            'description' => $permission->description
        ]);

        $this->assertEquals(201, $permissionController->create($request)->status());
    }

    public function testCreateWrongPermission()
    {
        $this->expectException(ValidationException::class);

        $permission = Permission::factory()->make();

        $permissionService = Mockery::mock(PermissionService::class);
        $roleService = Mockery::mock(RoleService::class);

        $permissionService->shouldReceive('create')->andReturn($permission);

        $permissionController = new  PermissionController($permissionService, $roleService);

        $request = new Request();

        $request->merge([
            'description' => $permission->description
        ]);

        $permissionController->create($request)->status();
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testUpdatePermission()
    {
        $permission = Permission::factory()->make();
        $permissionService = Mockery::mock(PermissionService::class);
        $roleService = Mockery::mock(RoleService::class);

        $permissionService->shouldReceive('update')->andReturn($permission);

        $permissionController = new  PermissionController($permissionService, $roleService);

        $request = new Request();

        $request->merge([
            'name' => $permission->name,
            'description' => $permission->description
        ]);

        $this->assertEquals(200, $permissionController->update($request, '1')->status());
    }

    /**
     * @throws Throwable
     */
    public function testExceptUpdatePermission()
    {

        $permission = Permission::factory()->make();
        $permissionService = Mockery::mock(PermissionService::class);
        $roleService = Mockery::mock(RoleService::class);

        $permissionService->shouldReceive('update')->andReturn($permission);

        $permissionController = new  PermissionController($permissionService, $roleService);

        $request = new Request();

        $request->merge([
            'description' => str_repeat('description', 50),
        ]);

        $this->expectException(ValidationException::class);

        $permissionController->update($request, '1');
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testReplacePermission()
    {
        $permission = Permission::factory()->make();

        $permissionService = Mockery::mock(PermissionService::class);
        $roleService = Mockery::mock(RoleService::class);

        $permissionService->shouldReceive('update')->andReturn($permission);

        $permissionController = new  PermissionController($permissionService, $roleService);

        $request = new Request();

        $request->merge([
            'name' => $permission->name,
            'description' => $permission->description
        ]);

        $this->assertEquals(200, $permissionController->replace($request, '1')->status());
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testReplaceWrongPermission()
    {

        $this->expectException(ModelNotFoundException::class);

        $permission = Permission::factory()->make();

        $permissionService = Mockery::mock(PermissionService::class);
        $roleService = Mockery::mock(RoleService::class);

        $permissionService->shouldReceive('update')->andReturn(Null);

        $permissionController = new  PermissionController($permissionService, $roleService);

        $request = new Request();

        $request->merge([
            'name' => $permission->name,
            'description' => $permission->description
        ]);

        $permissionController->replace($request, '1')->status();
    }

    /**
     * @throws Throwable
     */
    public function testGivePermission()
    {
        $role = Role::factory()->make();
        $permission = Role::factory()->make();

        $permissionService = Mockery::mock(PermissionService::class);
        $roleService = Mockery::mock(RoleService::class);

        $roleService->shouldReceive('find')->andReturn($role);
        $permissionService->shouldReceive('find')->andReturn($permission);
        $permissionService->shouldReceive('hasRolePermission')->andReturn(False);
        $permissionService->shouldReceive('givePermission')->andReturn(True);

        $permissionController = new  PermissionController($permissionService, $roleService);

        $this->assertEquals(200, $permissionController->give($role->id, $permission->id)->status());

    }

    /**
     * @throws Throwable
     */
    public function testGivePermissionUnknownRole()
    {
        $this->expectException(ModelNotFoundException::class);

        $role = Role::factory()->make();
        $permission = Role::factory()->make();

        $permissionService = Mockery::mock(PermissionService::class);
        $roleService = Mockery::mock(RoleService::class);

        $roleService->shouldReceive('find')->andReturn(Null);
        $permissionService->shouldReceive('find')->andReturn($permission);
        $permissionService->shouldReceive('hasRolePermission')->andReturn(False);
        $permissionService->shouldReceive('givePermission')->andReturn(True);

        $permissionController = new  PermissionController($permissionService, $roleService);

        $permissionController->give($role->id, $permission->id);

    }

    /**
     * @throws Throwable
     */
    public function testGivePermissionUnknownPermission()
    {
        $this->expectException(ModelNotFoundException::class);

        $role = Role::factory()->make();
        $permission = Role::factory()->make();

        $permissionService = Mockery::mock(PermissionService::class);
        $roleService = Mockery::mock(RoleService::class);

        $roleService->shouldReceive('find')->andReturn($role);
        $permissionService->shouldReceive('find')->andReturn(Null);
        $permissionService->shouldReceive('hasRolePermission')->andReturn(False);
        $permissionService->shouldReceive('givePermission')->andReturn(True);

        $permissionController = new  PermissionController($permissionService, $roleService);

        $permissionController->give($role->id, $permission->id);

    }

    /**
     * @throws Throwable
     */
    public function testGiveExistPermission()
    {
        $this->expectException(ENTITY_EXISTS_EXCEPTION::class);

        $role = Role::factory()->make();
        $permission = Role::factory()->make();

        $permissionService = Mockery::mock(PermissionService::class);
        $roleService = Mockery::mock(RoleService::class);

        $roleService->shouldReceive('find')->andReturn($role);
        $permissionService->shouldReceive('find')->andReturn($permission);
        $permissionService->shouldReceive('hasRolePermission')->andReturn(True);
        $permissionService->shouldReceive('givePermission')->andReturn(True);

        $permissionController = new  PermissionController($permissionService, $roleService);

        $permissionController->give($role->id, $permission->id);

    }

    /**
     * @throws Throwable
     */
    public function testRemovePermission()
    {
        $role = Role::factory()->make();
        $permission = Role::factory()->make();

        $permissionService = Mockery::mock(PermissionService::class);
        $roleService = Mockery::mock(RoleService::class);


        $roleService->shouldReceive('find')->andReturn($role);
        $permissionService->shouldReceive('find')->andReturn($permission);
        $permissionService->shouldReceive('hasRolePermission')->andReturn(True);
        $permissionService->shouldReceive('removePermission')->andReturn(True);

        $permissionController = new  PermissionController($permissionService, $roleService);

        $this->assertEquals(200, $permissionController->remove($role->id, $permission->id)->status());
    }

    /**
     * @throws Throwable
     */
    public function testRemovePermissionUnknownRole()
    {
        $this->expectException(ModelNotFoundException::class);

        $role = Role::factory()->make();
        $permission = Role::factory()->make();

        $permissionService = Mockery::mock(PermissionService::class);
        $roleService = Mockery::mock(RoleService::class);


        $roleService->shouldReceive('find')->andReturn(Null);
        $permissionService->shouldReceive('find')->andReturn($permission);
        $permissionService->shouldReceive('hasRolePermission')->andReturn(True);
        $permissionService->shouldReceive('removePermission')->andReturn(True);

        $permissionController = new  PermissionController($permissionService, $roleService);

        $permissionController->remove($role->id, $permission->id);
    }

    /**
     * @throws Throwable
     */
    public function testRemovePermissionUnknownPermission()
    {
        $this->expectException(ModelNotFoundException::class);

        $role = Role::factory()->make();
        $permission = Role::factory()->make();

        $permissionService = Mockery::mock(PermissionService::class);
        $roleService = Mockery::mock(RoleService::class);


        $roleService->shouldReceive('find')->andReturn($role);
        $permissionService->shouldReceive('find')->andReturn(Null);
        $permissionService->shouldReceive('hasRolePermission')->andReturn(True);
        $permissionService->shouldReceive('removePermission')->andReturn(True);

        $permissionController = new  PermissionController($permissionService, $roleService);

        $permissionController->remove($role->id, $permission->id);
    }

    /**
     * @throws Throwable
     */
    public function testRemoveNonExistentPermission()
    {
        $this->expectException(ModelNotFoundException::class);

        $role = Role::factory()->make();
        $permission = Role::factory()->make();

        $permissionService = Mockery::mock(PermissionService::class);
        $roleService = Mockery::mock(RoleService::class);


        $roleService->shouldReceive('find')->andReturn($role);
        $permissionService->shouldReceive('find')->andReturn(Null);
        $permissionService->shouldReceive('hasRolePermission')->andReturn(True);
        $permissionService->shouldReceive('removePermission')->andReturn(True);

        $permissionController = new  PermissionController($permissionService, $roleService);

        $this->assertEquals(200, $permissionController->remove($role->id, $permission->id)->status());
    }
}

