<?php

use Laravel\Lumen\Routing\Router;

/** @var Router $router */

$router->get('/', function () {
    return ['api' => env('APP_NAME')];
});

###
$router->get('/role/{role_id}', ['middleware' => 'authorization:view-role', 'uses' => 'RoleController@item']);
$router->delete('/role/{role_id}', ['middleware' => 'authorization:delete-role', 'uses' => 'RoleController@delete']);
$router->post('/role', ['middleware' => 'authorization:create-role', 'uses' => 'RoleController@create']);
$router->get('/role/{role_id}/permissions', ['middleware' => 'authorization:view-role-permissions', 'uses' => 'PermissionController@list']);
$router->put('/role/{id}', ['middleware' => 'authorization:replace-role', 'uses' => 'RoleController@replace']);
$router->patch('/role/{id}', ['middleware' => 'authorization:update-role', 'uses' => 'RoleController@update']);
$router->get('/roles', ['middleware' => 'authorization:view-roles', 'uses' => 'RoleController@all']);

$router->get('/permission/{permission_id}', ['middleware' => 'authorization:view-permission', 'uses' => 'PermissionController@item']);
$router->delete('/permission/{permission_id}', ['middleware' => 'authorization:delete-permission', 'uses' => 'PermissionController@delete']);
$router->post('/permission', ['middleware' => 'authorization:create-permission', 'uses' => 'PermissionController@create']);
$router->put('/permission/{id}', ['middleware' => 'authorization:replace-permission', 'uses' => 'PermissionController@replace']);
$router->patch('/permission/{id}', ['middleware' => 'authorization:update-permission', 'uses' => 'PermissionController@update']);
$router->get('/permissions', ['middleware' => 'authorization:view-permissions', 'uses' => 'PermissionController@all']);

$router->get('/role/{role_id}/permission/{permission_id}', ['middleware' => 'authorization:give-permission', 'uses' => 'PermissionController@give']);
$router->delete('/role/{role_id}/permission/{permission_id}', ['middleware' => 'authorization:remove-permission', 'uses' => 'PermissionController@remove']);

$router->get('/roles?filter="user_uuid:{user_uuid}"', ['middleware' => 'authorization:view-user-roles', 'uses' => 'RoleController@list']);
$router->post('/user/{user_uuid}/role/{role_id}', ['middleware' => 'authorization:give-role', 'uses' => 'RoleController@give']);
$router->delete('/user/{user_uuid}/role/{role_id}', ['middleware' => 'authorization:remove-role', 'uses' => 'RoleController@remove']);


$router->post('/user','AuthMicroserviceLogicController@register');

// TODO: ТЕСТОВЫЙ МАРШРУТ. УБРАТЬ ПРИ ДЕПЛОЕ! ВЫДАЕТ АДМИНКУ ПОЛЬЗОВАТЕЛЮ
 $router->get('/admin', ['middleware' => 'authentication:true', 'uses' => 'RoleController@admin']);

