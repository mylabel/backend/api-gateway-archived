<?php

namespace Database\Factories;

use App\Models\Role;
use Illuminate\Database\Eloquent\Factories\Factory;
use JetBrains\PhpStorm\ArrayShape;

class RoleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Role::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    #[ArrayShape(['name' => "string", 'access_level' => "int", 'description' => "string"])] public function definition(): array
    {
        return [
            'name' => 'testable_' . $this->faker->word(),
            'access_level' => 1,
            'description' => $this->faker->sentence(),
        ];
    }
}
